package edu.upi.cs.beconnect.beconnect.Api;

import edu.upi.cs.beconnect.beconnect.Model.Data;
import edu.upi.cs.beconnect.beconnect.Model.Register;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PatraApiInterface {
    String ACCESS_KEY = "ec764af48e11933becd01be16223d9";

    @Headers("Access-key: "+ACCESS_KEY)
    @FormUrlEncoded
    @POST("client/register")
    Call<PatraApiData<Register>> registerAccount(@Field("client_first_name") String first_name,
                                                 @Field("client_last_name") String last_name,
                                                 @Field("client_email") String email,
                                                 @Field("client_password") String password,
                                                 @Field("client_password_confirmation") String password_confirm,
                                                 @Field("company_name") String company_name) ;

    @Headers("Access-key: "+ACCESS_KEY)
    @FormUrlEncoded
    @POST("client/login")
    Call<PatraApiData<Data>> loginAccount(@Field("username") String username,
                                          @Field("password") String password);
}

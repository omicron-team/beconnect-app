package edu.upi.cs.beconnect.beconnect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import edu.upi.cs.beconnect.beconnect.Model.Tenant;
import edu.upi.cs.beconnect.beconnect.REST.ApiClient;
import edu.upi.cs.beconnect.beconnect.REST.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TenantDetailActivity extends AppCompatActivity {
    private Tenant tenants;
    private static String TAG = MapsActivity.class.getSimpleName();
    private ImageView im_tenant;
    private TextView tv_nama_tenant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenant_detail);

        im_tenant = findViewById(R.id.im_tenant_logo);
        tv_nama_tenant = findViewById(R.id.tv_name_tenant);
        Intent i = getIntent();
        String id = i.getStringExtra("id");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Tenant> call = apiService.getTenant(id);
        call.enqueue(new Callback<Tenant>() {
            @Override
            public void onResponse(Call<Tenant> call, Response<Tenant> response) {
                tenants = response.body();
                if (tenants!=null){
                    Glide.with(getBaseContext())
                            .asBitmap()
                            .load(tenants.getTenant_logo())
                            .into(im_tenant);
                    tv_nama_tenant.setText(tenants.getTenant_name());
                }
                Log.d(TAG, "onResponse: " + response.body());
            }

            @Override
            public void onFailure(Call<Tenant> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t);
            }
        });
    }
}

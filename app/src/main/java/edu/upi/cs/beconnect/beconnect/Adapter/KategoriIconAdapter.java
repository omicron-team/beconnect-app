package edu.upi.cs.beconnect.beconnect.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import edu.upi.cs.beconnect.beconnect.MapsActivity;
import edu.upi.cs.beconnect.beconnect.R;
import edu.upi.cs.beconnect.beconnect.model.KategoriIcon;

public class KategoriIconAdapter extends RecyclerView.Adapter<KategoriIconAdapter.ViewHolder> {

    private List<KategoriIcon> mData;
    private LayoutInflater mInflater;
    private Context context;

    // data is passed into the constructor
    public KategoriIconAdapter(Context context, List<KategoriIcon> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    @NonNull
    @Override
    public KategoriIconAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.layout_icon, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        KategoriIcon kategori = mData.get(i);

        viewHolder.name.setText(kategori.getName());
        viewHolder.kategoriId.setText(Integer.toString(kategori.getId()));
        Glide.with(context).asBitmap().apply(RequestOptions.circleCropTransform()).load(kategori.getImg()).into(viewHolder.imageKategori);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageKategori;
        TextView name, kategoriId;

        ViewHolder(View itemView){
            super(itemView);

            kategoriId = itemView.findViewById(R.id.id);
            name = itemView.findViewById(R.id.name);
            imageKategori = itemView.findViewById(R.id.logo);
            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(context, MapsActivity.class);
                intent.putExtra("sub_sector_id",kategoriId.getText().toString());
                Toast.makeText(context, kategoriId.getText(), Toast.LENGTH_SHORT).show();
                context.startActivity(intent);
            });
        }

    }
}

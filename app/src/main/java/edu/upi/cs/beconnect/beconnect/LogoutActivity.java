package edu.upi.cs.beconnect.beconnect;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;

public class LogoutActivity extends LoginActivity {

    private Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);

        Bundle bundle = getIntent().getExtras();

        TextView tv = findViewById(R.id.nama);
        tv.setText(bundle.getString("name"));

        btnLogout = findViewById(R.id.sign_out_button);
        btnLogout.setOnClickListener(view -> Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(status -> {
            googleApiClient.clearDefaultAccountAndReconnect();

            Intent intent = new Intent(getBaseContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }));

    }
}

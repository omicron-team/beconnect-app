package edu.upi.cs.beconnect.beconnect.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Collections;
import java.util.List;

import edu.upi.cs.beconnect.beconnect.R;
import edu.upi.cs.beconnect.beconnect.model.Rekomendasi;

public class RekomendasiAdapter extends RecyclerView.Adapter<RekomendasiAdapter.ViewHolder> {

    private List<Rekomendasi> mData = Collections.emptyList();
    private Context context;
    private LayoutInflater mInflater;

    public RekomendasiAdapter(Context context, List<Rekomendasi> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    @NonNull
    @Override
    public RekomendasiAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.layout_rekomendasi, viewGroup, false);
        return new RekomendasiAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RekomendasiAdapter.ViewHolder viewHolder, int i) {
        Rekomendasi rekomendasi = mData.get(i);
        Glide.with(context).asBitmap().apply(RequestOptions.fitCenterTransform()).load(rekomendasi.getImage()).into(viewHolder.imageKategori);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageKategori;
        public ViewHolder(View itemView){
            super(itemView);

            imageKategori = itemView.findViewById(R.id.logo);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "" + getItemId(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}

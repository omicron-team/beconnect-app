package edu.upi.cs.beconnect.beconnect.REST;

import java.util.List;

import edu.upi.cs.beconnect.beconnect.Model.Kategori;
import edu.upi.cs.beconnect.beconnect.Model.Tenant;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET("subsector")
    Call<List<Kategori>> getKategori();

    @GET("tenant/{id}")
    Call<Tenant> getTenant(@Path("id") String id);

    @GET("tenant/bySubsector/{id}")
    Call<List<Tenant>> getTenantBySubsectorId(@Path("id") String id);



}

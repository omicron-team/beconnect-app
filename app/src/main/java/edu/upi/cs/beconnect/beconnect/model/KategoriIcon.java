package edu.upi.cs.beconnect.beconnect.model;

public class KategoriIcon {
    private int id, img;
    private String name;

    public KategoriIcon(int id, String name, int img) {
        this.id = id;
        this.img = img;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

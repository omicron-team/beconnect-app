package edu.upi.cs.beconnect.beconnect;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailTempatActivity extends AppCompatActivity {

    private ImageView ivGambarCover, ivGambarLogo;
    private TextView tvNama, tvAlamat, tvDeskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tempat);

        ivGambarCover = findViewById(R.id.ivTempat);
        ivGambarLogo = findViewById(R.id.logoTempat);
        tvNama = findViewById(R.id.namaTempat);
        tvAlamat = findViewById(R.id.alamatTempat);
        tvDeskripsi = findViewById(R.id.deskripsiTempat);
    }
}

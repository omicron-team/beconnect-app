package edu.upi.cs.beconnect.beconnect.Model;

public class Register {
    private String app_username, app_email, is_active, updated_at, id;

    public Register() {
    }

    public Register(String app_username, String app_email, String is_active, String updated_at, String id) {
        this.app_username = app_username;
        this.app_email = app_email;
        this.is_active = is_active;
        this.updated_at = updated_at;
        this.id = id;
    }

    public String getApp_username() {
        return app_username;
    }

    public void setApp_username(String app_username) {
        this.app_username = app_username;
    }

    public String getApp_email() {
        return app_email;
    }

    public void setApp_email(String app_email) {
        this.app_email = app_email;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

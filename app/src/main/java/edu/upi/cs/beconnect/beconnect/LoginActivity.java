package edu.upi.cs.beconnect.beconnect;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import edu.upi.cs.beconnect.beconnect.Api.PatraApiClient;
import edu.upi.cs.beconnect.beconnect.Api.PatraApiData;
import edu.upi.cs.beconnect.beconnect.Api.PatraApiInterface;
import edu.upi.cs.beconnect.beconnect.Model.Data;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 9001;
    public GoogleSignInClient mGoogleSignInClient;
    public GoogleApiClient googleApiClient;
    public GoogleSignInAccount account;
    public Button LgnButton, RgsButton;
    private PatraApiData<Data> akun;
    private String name, email, img, bg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SignInButton signIn = findViewById(R.id.sign_in_button);
        signIn.setOnClickListener(view -> {
            switch (view.getId()){
                case R.id.sign_in_button:
                    signIn();
                    break;
            }
        });

        RgsButton = findViewById(R.id.register);
        RgsButton.setOnClickListener(v->{
            Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
            startActivity(intent);
        });

        LgnButton = findViewById(R.id.login);
        LgnButton.setOnClickListener(v->{
            EditText et_user, et_pass;
            et_user = findViewById(R.id.et_lg_user);
            et_pass = findViewById(R.id.et_lg_pass);
            String user = et_user.getText().toString().trim();
            String pass = et_pass.getText().toString().trim();

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            @SuppressLint("InflateParams")
            View view = getLayoutInflater().inflate(R.layout.progressbar, null);
            builder.setView(view);
            builder.setCancelable(false);
            final Dialog dialog = builder.create();

            //checking if email and passwords are empty
            if (TextUtils.isEmpty(user)) {
                Toast.makeText(this, "Tolong isi username atau email", Toast.LENGTH_LONG).show();
                return;
            }

            if (TextUtils.isEmpty(pass)) {
                Toast.makeText(this, "Tolong isi password", Toast.LENGTH_LONG).show();
                return;
            }
            dialog.show();
            final Context c = this;
            PatraApiInterface apiService = PatraApiClient.getClient().create(PatraApiInterface.class);
            Call<PatraApiData<Data>> call = apiService.loginAccount(user, pass);
            call.enqueue(new Callback<PatraApiData<Data>>() {
                @Override
                public void onResponse(@NonNull Call<PatraApiData<Data>> call, @NonNull Response<PatraApiData<Data>> response) {
                    akun = response.body();
                    dialog.dismiss();

                    if (akun != null) {
                        if (akun.getMessage().equals("success")){

                            Toast.makeText(c, "Berhasil Masuk", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else{
                            Toast.makeText(c, "Username atau email dan password salah", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                @Override
                public void onFailure(@NonNull Call<PatraApiData<Data>> call, @NonNull Throwable t) {
                    dialog.dismiss();
                    Log.e(TAG, "onFailure: ", t);
                    Toast.makeText(c, "connection error", Toast.LENGTH_LONG).show();
                }
            });


        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


    }

    private void signIn(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void updateUI(boolean isLogin){
        if (isLogin){
            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
            intent.putExtra("name", this.name);
            intent.putExtra("email", this.email);
            intent.putExtra("img", this.img);
            startActivity(intent);
            finish();
        }else{
            Toast toast = Toast.makeText(getApplicationContext(), "Data Gagal.", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void handleResult(GoogleSignInResult result){
        if (result.isSuccess()){
            account = result.getSignInAccount();

            name = account.getDisplayName();
            email = account.getEmail();
            img = account.getPhotoUrl().toString();
            updateUI(true);
        }
        else{
            updateUI(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }
}

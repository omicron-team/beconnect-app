package edu.upi.cs.beconnect.beconnect.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import edu.upi.cs.beconnect.beconnect.MapsActivity;
import edu.upi.cs.beconnect.beconnect.R;
import edu.upi.cs.beconnect.beconnect.Model.Kategori;

public class RvKategoriAdapter extends RecyclerView.Adapter<RvKategoriAdapter.ViewHolder> {

    private List<Kategori> mData;
    private LayoutInflater mInflater;
    private Context context;

    // data is passed into the constructor
    public RvKategoriAdapter(Context context, List<Kategori> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    @NonNull
    @Override
    public RvKategoriAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.layout_kategori, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Kategori kategori = mData.get(i);

        viewHolder.nama.setText(kategori.getSub_sector_name());
        viewHolder.id.setText(Integer.toString(kategori.getId()));
        Glide.with(context).asBitmap().apply(RequestOptions.circleCropTransform()).load(kategori.getSub_sector_img()).into(viewHolder.imageKategori);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageKategori;
        TextView nama, id;

        ViewHolder(View itemView){
            super(itemView);

            id = itemView.findViewById(R.id.idKategori);
            nama = itemView.findViewById(R.id.tvNama);
            imageKategori = itemView.findViewById(R.id.logo);
            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(context, MapsActivity.class);
                intent.putExtra("sub_sector_id",id.getText().toString());
                Toast.makeText(context, id.getText(), Toast.LENGTH_SHORT).show();
                context.startActivity(intent);
            });
        }

    }
}

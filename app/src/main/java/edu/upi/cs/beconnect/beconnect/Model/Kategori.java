package edu.upi.cs.beconnect.beconnect.Model;

public class Kategori {
    private int id;
    private String sub_sector_name, sub_sector_description, sub_sector_img, sub_sector_slug;

    public Kategori() {
    }

    public Kategori(int id, String sub_sector_name, String sub_sector_description, String sub_sector_img, String sub_sector_slug) {
        this.id = id;
        this.sub_sector_name = sub_sector_name;
        this.sub_sector_description = sub_sector_description;
        this.sub_sector_img = sub_sector_img;
        this.sub_sector_slug = sub_sector_slug;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSub_sector_name() {
        return sub_sector_name;
    }

    public void setSub_sector_name(String sub_sector_name) {
        this.sub_sector_name = sub_sector_name;
    }

    public String getSub_sector_description() {
        return sub_sector_description;
    }

    public void setSub_sector_description(String sub_sector_description) {
        this.sub_sector_description = sub_sector_description;
    }

    public String getSub_sector_img() {
        return sub_sector_img;
    }

    public void setSub_sector_img(String sub_sector_img) {
        this.sub_sector_img = sub_sector_img;
    }

    public String getSub_sector_slug() {
        return sub_sector_slug;
    }

    public void setSub_sector_slug(String sub_sector_slug) {
        this.sub_sector_slug = sub_sector_slug;
    }
}

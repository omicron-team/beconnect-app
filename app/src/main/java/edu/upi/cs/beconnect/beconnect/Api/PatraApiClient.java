package edu.upi.cs.beconnect.beconnect.Api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PatraApiClient {

    public static final String BASE_URL = "http://patrakomala.disbudpar.bandung.go.id:8080/api/v1/public/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

}
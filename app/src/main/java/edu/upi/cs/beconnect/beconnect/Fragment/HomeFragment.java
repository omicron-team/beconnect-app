package edu.upi.cs.beconnect.beconnect.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.michaelgarnerdev.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.upi.cs.beconnect.beconnect.Adapter.KategoriIconAdapter;
import edu.upi.cs.beconnect.beconnect.Adapter.RekomendasiAdapter;
import edu.upi.cs.beconnect.beconnect.Adapter.RvKategoriAdapter;
import edu.upi.cs.beconnect.beconnect.R;
import edu.upi.cs.beconnect.beconnect.model.KategoriIcon;
import edu.upi.cs.beconnect.beconnect.model.Rekomendasi;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private List<Rekomendasi> rekomendasiList = Collections.emptyList();
    private RekomendasiAdapter adapter;
    private RecyclerView recyclerView;

    private List<KategoriIcon> kategoriList;
    private KategoriIconAdapter adapter1;
    private RecyclerView recyclerView1;

    MaterialSearchView material_search_view;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        material_search_view = view.findViewById(R.id.material_search_view);
        material_search_view.setHintText(R.string.search_string);

        rekomendasiList = new ArrayList<>();
        rekomendasiList.add(new Rekomendasi(1," ",R.drawable.slide_1));
        rekomendasiList.add(new Rekomendasi(2," ",R.drawable.slide_2));
        rekomendasiList.add(new Rekomendasi(3," ",R.drawable.slide_3));
        rekomendasiList.add(new Rekomendasi(4," ",R.drawable.slide_4));

        adapter = new RekomendasiAdapter(getContext(), rekomendasiList);

        recyclerView = view.findViewById(R.id.rekomendasi);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));


        recyclerView.setAdapter(adapter);
        SnapHelper snapHelperStart = new GravitySnapHelper(Gravity.START);
        snapHelperStart.attachToRecyclerView(recyclerView);



        //Recycler View Divider

        kategoriList = new ArrayList<>();
        kategoriList.add(new KategoriIcon(1,"Aplikasi",R.drawable.aplikasi_1));
        kategoriList.add(new KategoriIcon(2,"Arsitek",R.drawable.arsitek_2));
        kategoriList.add(new KategoriIcon(3,"Desain Interior",R.drawable.desain_interior_3));
        kategoriList.add(new KategoriIcon(4,"DKV",R.drawable.dkv_4));
        kategoriList.add(new KategoriIcon(5,"Desain Produk",R.drawable.desain_produk_5));
        kategoriList.add(new KategoriIcon(6,"Fashion",R.drawable.fashion_6));
        kategoriList.add(new KategoriIcon(7,"Film",R.drawable.film_7));
        kategoriList.add(new KategoriIcon(8,"Fotografi",R.drawable.fotografi_8));
        kategoriList.add(new KategoriIcon(9,"Kriya",R.drawable.kriya_9));
        kategoriList.add(new KategoriIcon(10,"Kuliner",R.drawable.kuliner_10));
        kategoriList.add(new KategoriIcon(11,"Desain Interior",R.drawable.musik_11));
        kategoriList.add(new KategoriIcon(12,"Penerbitan",R.drawable.penerbitan_12));
        kategoriList.add(new KategoriIcon(13,"Periklanan",R.drawable.periklanan_12));
        kategoriList.add(new KategoriIcon(14,"Pertunjukan",R.drawable.seni_pertunjukan_14));
        kategoriList.add(new KategoriIcon(15,"Seni Rupa",R.drawable.seni_rupa_15));
        kategoriList.add(new KategoriIcon(16,"Televisi",R.drawable.televisi_16));
        adapter1 = new KategoriIconAdapter(getContext(), kategoriList);

        recyclerView1 = view.findViewById(R.id.kategori);
        recyclerView1.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));

        recyclerView1.setAdapter(adapter1);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView1);



        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("BECONNECT");



    }

}

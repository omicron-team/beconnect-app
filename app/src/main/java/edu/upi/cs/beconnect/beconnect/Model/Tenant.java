package edu.upi.cs.beconnect.beconnect.Model;

public class Tenant {
    private int id, sub_sector_id;
    private String tenant_name, tenant_logo, latitude, longitude;

    public Tenant() {
    }

    public Tenant(int id, int sub_sector_id, String tenant_name, String tenant_logo, String latitude, String longitude) {
        this.id = id;
        this.sub_sector_id = sub_sector_id;
        this.tenant_name = tenant_name;
        this.tenant_logo = tenant_logo;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSub_sector_id() {
        return sub_sector_id;
    }

    public void setSub_sector_id(int sub_sector_id) {
        this.sub_sector_id = sub_sector_id;
    }

    public String getTenant_name() {
        return tenant_name;
    }

    public void setTenant_name(String tenant_name) {
        this.tenant_name = tenant_name;
    }

    public String getTenant_logo() {
        return tenant_logo;
    }

    public void setTenant_logo(String tenant_logo) {
        this.tenant_logo = tenant_logo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}

package edu.upi.cs.beconnect.beconnect;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.upi.cs.beconnect.beconnect.Api.PatraApiClient;
import edu.upi.cs.beconnect.beconnect.Api.PatraApiData;
import edu.upi.cs.beconnect.beconnect.Api.PatraApiInterface;
import edu.upi.cs.beconnect.beconnect.Model.Register;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private EditText et_fir_nam, et_last_name, et_email, et_pass, et_pass_conf, et_comp;
    private PatraApiData<Register> regis;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        et_fir_nam = findViewById(R.id.et_rg_first_nam);
        et_last_name = findViewById(R.id.et_rg_last_name);
        et_email = findViewById(R.id.et_rg_email);
        et_pass = findViewById(R.id.et_rg_password);
        et_pass_conf = findViewById(R.id.et_rg_password_conf);
        et_comp = findViewById(R.id.et_rg_company_name);

        Button bt_regis = findViewById(R.id.bt_rg_register);
        bt_regis.setOnClickListener(v->{
            String first_name = et_fir_nam.getText().toString().trim();
            String last_name = et_last_name.getText().toString().trim();
            String email = et_email.getText().toString().trim();
            String password = et_pass.getText().toString().trim();
            String pass_conf = et_pass_conf.getText().toString().trim();
            String company = et_comp.getText().toString().trim();
            if (TextUtils.isEmpty(first_name)) {
                Toast.makeText(this, "Tolong isi nama anda", Toast.LENGTH_LONG).show();
                return;
            }

            if (TextUtils.isEmpty(last_name)) {
                Toast.makeText(this, "Tolong isi nama anda", Toast.LENGTH_LONG).show();
                return;
            }
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(this, "Tolong isi email anda", Toast.LENGTH_LONG).show();
                return;
            }
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(this, "Tolong isi password anda", Toast.LENGTH_LONG).show();
                return;
            }
            if (TextUtils.isEmpty(pass_conf)) {
                Toast.makeText(this, "Tolong isi password anda", Toast.LENGTH_LONG).show();
                return;
            }
            if (TextUtils.isEmpty(company)) {
                Toast.makeText(this, "Tolong isi perusahaan anda", Toast.LENGTH_LONG).show();
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            @SuppressLint("InflateParams")
            View view = getLayoutInflater().inflate(R.layout.progressbar, null);
            builder.setView(view);
            builder.setCancelable(false);
            final Dialog dialog = builder.create();

            dialog.show();
            final Context c = this;
            PatraApiInterface apiService = PatraApiClient.getClient().create(PatraApiInterface.class);
            Call<PatraApiData<Register>> call = apiService.registerAccount(first_name, last_name, email, password, pass_conf, company);
            call.enqueue(new Callback<PatraApiData<Register>>() {
                @Override
                public void onResponse(@NonNull Call<PatraApiData<Register>> call, @NonNull Response<PatraApiData<Register>> response) {
                    regis = response.body();
                    dialog.dismiss();

                    if (regis != null) {
                        if (regis.getMessage().equals("Created")){

                            Toast.makeText(c, "Berhasil Daftar", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else{
                            Toast.makeText(c, "Ada Kesalahan", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                @Override
                public void onFailure(@NonNull Call<PatraApiData<Register>> call, @NonNull Throwable t) {
                    dialog.dismiss();
                    Log.e(TAG, "onFailure: ", t);
                    Toast.makeText(c, "connection error", Toast.LENGTH_LONG).show();
                }
            });
        });
    }
}

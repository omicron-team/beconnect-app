package edu.upi.cs.beconnect.beconnect.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;

import com.bumptech.glide.Glide;

import java.util.List;

import edu.upi.cs.beconnect.beconnect.Adapter.RvKategoriAdapter;
import edu.upi.cs.beconnect.beconnect.R;
import edu.upi.cs.beconnect.beconnect.REST.ApiClient;
import edu.upi.cs.beconnect.beconnect.REST.ApiInterface;
import edu.upi.cs.beconnect.beconnect.Model.Kategori;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {

    private static String TAG = CategoryFragment.class.getSimpleName();
    private List<Kategori> kategoriList;
    private RvKategoriAdapter adapter;
    private RecyclerView recyclerView;

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);

        recyclerView = view.findViewById(R.id.rvKategori);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Kategori>> call = apiService.getKategori();
        call.enqueue(new Callback<List<Kategori>>() {
            @Override
            public void onResponse(Call<List<Kategori>> call, Response<List<Kategori>> response) {
                kategoriList = response.body();
                if (kategoriList != null) {
                    for(Kategori k : kategoriList){
                        k.setSub_sector_img("https://beconnect-service.herokuapp.com/api/imgkategori/"+k.getId());
                    }
                    adapter = new RvKategoriAdapter(getContext(), kategoriList);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Kategori>> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Kategori");
    }

    private void getKategoriData() {

    }
}

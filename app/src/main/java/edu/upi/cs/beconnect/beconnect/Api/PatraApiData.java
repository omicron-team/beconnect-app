package edu.upi.cs.beconnect.beconnect.Api;

import java.util.List;

public class PatraApiData<T> {
    private String status, message;
    private List<String> errors;
    private T data;

    public PatraApiData() {
    }

    public PatraApiData(String status, String message, List<String> errors, T data) {
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
